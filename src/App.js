import React from "react";
import Navbar from "./Components/Navbar";
import MainRoutes from "./Routing/MainRoutes";
import { createTheme, ThemeProvider } from '@mui/material';

const theme = createTheme({
  typography: {
    fontFamily: [
      'Roboto Slab', 
      'serif',
      'Ubuntu'
    ].join(',')
  },
  palette:{
    primary: {
      main: process.env.REACT_APP_COLOR_PRIMARY
    }
  }
});

function App() {
  return (
    <>
    <ThemeProvider theme={theme}>
    <Navbar/>
    <MainRoutes/>
    </ThemeProvider>
    </>
  );
}

export default App;
